drop table if exists customer;
drop table if exists bills;
drop table if exists billitems;
drop table if exists products;

Create Table if not exists customer
(
custid text primary key,
cname text not null
);

Create table bills 
(
custid text,
billno primary key,
foreign key(custid) references customer(custid) on delete restrict on update cascade
);

create table billitems
(
billno text,
srno text,
prodid text,
qty int,
primary key(billno,srno),
foreign key(billno) references bills(billno) on delete restrict on update cascade,
foreign key(prodid) references products(prodid) on delete restrict on update cascade
);

create table products
(
prodid text primary key,
prodname text ,
price int
);

insert into customer values('c1','Sachin Tend');
insert into customer values('c2','Amitabh Bacc');
insert into products values('p12','lifegirl 75gm','250' );
insert into products values('p13','deadboy 75 gm','150');
insert into products values('p31','brookbond tea','540');
insert into products values('p51','raymond incomplete grandpa shirt','92');
insert into billitems values('b101','1','p12',5);
insert into billitems values('b101','2','p13',4);
insert into billitems values('b101','3','p31',2);
insert into billitems values('b102','1','p12',10);
insert into billitems values('b102','2','p51',5);
insert into billitems values('b102','3','p31',10);
insert into bills values('c1','b101');
insert into bills values('c2','b121');
insert into bills values('c2','b124');

Queries:
1.
2.
sqlite> select billno,sum(totamt) from (select billno,(products.price*billitems.qty) as totamt from products,billitems where products.prodid=billitems.prodid)group by billno ;
3.
sqlite> select pid,sum(totamt) from(select products.prodid as pid,(products.price*billitems.qty) as totamt from products,billitems where products.prodid=billitems.prodid) group by pid;
4.
